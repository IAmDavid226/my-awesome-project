package api.client;

import application.client.ClientService;

public class ClientResource {

	private ClientService clientService;

	public ClientResource(ClientService clientService) {
		this.clientService = clientService;
	}

	public void createAccount() {
		clientService.createAccount();
	}
}
