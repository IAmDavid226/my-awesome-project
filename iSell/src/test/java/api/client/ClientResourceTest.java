package api.client;

import application.client.ClientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class ClientResourceTest {

	private ClientResource clientResource;
	@Mock
	private ClientService clientService;

	@Before
	public void setUp(){
		 clientResource = new ClientResource(clientService);
	}

	@Test
	public void whenCreatingAAccount_thenDelegateToTheService(){
		clientResource.createAccount();

		verify(clientService).createAccount();
	}
}
